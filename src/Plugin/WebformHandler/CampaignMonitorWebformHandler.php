<?php

namespace Drupal\vb_campaignmonitor\Plugin\WebformHandler;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\TrustedRedirectResponse;
use Drupal\Core\Url;
use Drupal\webform\Plugin\WebformHandlerBase;
use Drupal\webform\WebformSubmissionInterface;

require_once '../vendor/campaignmonitor/createsend-php/csrest_clients.php';
require_once '../vendor/campaignmonitor/createsend-php/csrest_general.php';

/**
 * Submits .
 *
 * @WebformHandler(
 *   id = "campaignmonitor_webform_handler",
 *   label = @Translation("CampaignMonitor Webform Handler"),
 *   category = @Translation("Transaction"),
 *   description = @Translation("Sends the submission data to CampaignMonitor."),
 *   cardinality = \Drupal\webform\Plugin\WebformHandlerInterface::CARDINALITY_UNLIMITED,
 *   results = \Drupal\webform\Plugin\WebformHandlerInterface::RESULTS_PROCESSED,
 *   submission = \Drupal\webform\Plugin\WebformHandlerInterface::SUBMISSION_OPTIONAL,
 * )
 */
class CampaignMonitorWebformHandler extends WebformHandlerBase {
  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'list_id' => '',
      'email' => '',
      'name' => '',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $cm_config = \Drupal::config('vb_campaignmonitor.settings');
    $client_id = $cm_config->get('client_id');
    if(empty($client_id)) {
      $wrap = new \CS_REST_General($cm_config->get('api_key'));
      $result = $wrap->get_clients();
      if($result->was_successful() && isset($result->response[0]->ClientID)) {
        $client_id = $result->response[0]->ClientID;
      }
    }
    $form['list_id'] = [
      '#type' => 'select',
      '#title' => $this->t('List'),
      '#options' => ['' => $this->t('Select a list')],
      '#weight' => 0,
      '#required' => TRUE,
    ];
    $wrap = new \CS_REST_Clients($client_id, $cm_config->get('api_key'));
    $result = $wrap->get_lists();
    if($result->was_successful()) {
      foreach($result->response as $list) {
        $form['list_id']['#options'][$list->ListID] = $list->Name;
      }
    }
    if(isset($form['list_id']['#options'][$this->configuration['list_id']])) {
      $form['list_id']['#default_value'] = $this->configuration['list_id'];
    }
    if(!empty($this->configuration['list_id'])) {
      $elements = $this->webform->getElementsDecodedAndFlattened();
      foreach($elements as $key => $element) {
        if(!in_array($element['#type'], ['textfield', 'email', 'hidden', 'value'])) {
          unset($elements[$key]);
        }
      }
      $options = [];
      foreach($elements as $key => $element) {
        $options[$key] = $element['#title'];
      }
      $form['email'] = [
        '#type' => 'select',
        '#title' => $this->t('Email'),
        '#options' => $options,
        '#required' => TRUE,
        '#default_value' => $this->configuration['email'],
      ];
      $form['name'] = [
        '#type' => 'select',
        '#title' => $this->t('Full name'),
        '#options' => ['' => ''] + $options,
        '#default_value' => $this->configuration['name'],
      ];
      $wrap = new \CS_REST_Lists($this->configuration['list_id'], $cm_config->get('api_key'));
      $result = $wrap->get_custom_fields();
      if($result->was_successful() && is_array($result->response)) {
        foreach($result->response as $response) {
          $key = str_replace(['[', ']'], '', strtolower($response->Key));
          $form[$key] = [
            '#type' => 'select',
            '#title' => $response->FieldName,
            '#options' => ['' => ''] + $options,
            '#default_value' => $this->configuration[$key],
          ];
        }
      }
    }
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $this->configuration['list_id'] = $form_state->getValue('list_id');
    $this->configuration['email'] = $form_state->getValue('email');
    $this->configuration['name'] = $form_state->getValue('name');
    if(!empty($this->configuration['list_id'])) {
      $cm_config = \Drupal::config('vb_campaignmonitor.settings');
      $wrap = new \CS_REST_Lists($this->configuration['list_id'], $cm_config->get('api_key'));
      $result = $wrap->get_custom_fields();
      if($result->was_successful() && is_array($result->response)) {
        foreach($result->response as $response) {
          $key = str_replace(['[', ']'], '', strtolower($response->Key));
          $this->configuration[$key] = $form_state->getValue($key);
        }
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state,  WebformSubmissionInterface $webform_submission) {
    $cm_config = \Drupal::config('vb_campaignmonitor.settings');
    $values = $webform_submission->getData();
    $wrap = new \CS_REST_Lists($this->configuration['list_id'], $cm_config->get('api_key'));
    $result = $wrap->get_custom_fields();
    $keys = [];
    if($result->was_successful() && is_array($result->response)) {
      foreach($result->response as $response) {
        $keys[$response->Key] = str_replace(['[', ']'], '', strtolower($response->Key));
      }
    }
    $wrap = new \CS_REST_Subscribers($this->configuration['list_id'], $cm_config->get('api_key'));
    $data = [
      'EmailAddress' => $values[$this->configuration['email']],
      'ConsentToTrack' => 'unchanged',
      'Resubscribe' => TRUE,
      'CustomFields' => [],
    ];
    if(!empty($this->configuration['name'])) {
      $data['Name'] = $values[$this->configuration['name']];
    }
    foreach($keys as $cm_key => $key) {
      if(!empty($values[$this->configuration[$key]])) {
        $data['CustomFields'][] = [
          'Key' => $cm_key,
          'Value' => $values[$this->configuration[$key]],
        ];
      }
    }
    $result = $wrap->add($data);
  }
}
